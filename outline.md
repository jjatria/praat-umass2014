# Variables

    string$ = "He said ""string"" again"
    numeric = 42
    arrays# <- these are undocumented

The closest thing to arrays in Praat are "indexed variables", which do not seem
to be arrays, because they do not represent a data structure: they are
individual variables with a part of their name which is common.

    index[1]  = 1
    index[2]  = undefined
    index[10] = 10
    # variable index[1] and index[2] exist, but not index[3]

# Comments

    # Full line comment
    a = 10 ; Inline comment

# Conditions

Praat has boolean values, which allows it to generate conditions. The `assert`
directive interrupts execution when it encounters a false value, and is useful
for testing.

Only numeric values can be tested as conditions. A value of 0 (zero) will be
always false, any other value will be true.

    assert !0
    assert  1
    assert -1

Strings do not evaluate to booleans, but can be used in the generation of
conditional statements

    name$ = "John"
    if name$ != ""
      appendInfoLine: "Hello, ", name$
    endif

Praat will stop a script if an undefined values (or an unexisting variable) is
tested.

    # This will halt: "unknown variable"
    if newvar
    endif

    # This will also halt: undefined value
    newvar = undefined
    if newvar
    endif

There are three logical operators: `!` for negation, `&` for conjunction, and
`|` for disjunction. These can be replaced by `not`, `and`, and `or`.

Conditions in Praat short circuit, so that evaluation stops as soon as the value
is known. This makes it possible to make tests like

    a = undefined
    b = 4
    if b < 10 or b > a
      # This is fine
    endif

# Operators

Praat does not distinguish between `=` and `==`, but it accepts them to be used
separately as an assignment operator and an equality operator respectively.

In practice, not much code written for Praat keeps these separate.

    = == <>
    > >= <= <
    + - * /
    ^ mod div

    assert   1   =  1
    assert   1   == 1
    assert   1   <> 2
    assert   2   >  1
    assert   2   >= 2
    assert   1   <= 1
    assert   1   <  2
    assert   3   =  1 +  2
    assert   1   =  1 -  0
    assert  -1   =  1 * -1
    assert   1   =  1 /  1
    assert   16  =  2 ^ 4
    assert   1   =  13 mod 6
    assert   1   =  3  div 2
    assert !(1  !=  1)

# Control structures

## `for` loops

    for x [from y] to z
      # code
    endfor

## `if` blocks

    if cond1
      # code
    [elsif cond2
      # code]
    [else
      # code]
    endif

## `while` blocks

    while cond
      #code
    endwhile

## `repeat` blocks

    repeat
      #code
    until cond

# Functions

## String functions

## Mathematical functions

# Procedures

    procedure myproc [args]
      #code
    endproc

# Object selection

Praat uses an object-oriented design, in which certain entities are abstracted
as "objects" which can perform some actions, or on which some actions can be
performed. These actions are normally called "methods", or "member functions",
but Praat refers to them generally as "commands".

The benefit of this approach is that elements that share similarities can belong
to the same abstract class of objects, not only reducing the code that needs to
be written, but presenting a similar interface for similar actions.

A `Sound` and a `TextGrid` objects are different, and they represent different
types of things. But they do share some commonalities, since they both represent
a sequence in time. For Praat, this means that they both are a "time-based
subclass" of the Function class[^function].

[^function]: Which itself represents "a function `f(x, ...)` on the domain
`[xmin, xmax]`".

In practice, this means that both `Sound` and `TextGrid` objects have the same
interface for eg. querying their start and end, or their durations in time.

Since different methods (ie, "commands") are available to different classes of
objects, it is important to know what the active objects are, and this is why we
need to pay attention to the current selection.

A script will inherit the selection that was active when the script was run, but
this selection will likely not stay the same throughout. We can change it by
selecting different objects, or by adding or removing specific objects from our
selection. The selection will automatically change if a new object is created
(in which case that object will be selected) or if the selected objects are
deleted.

References to objects can be made using object names (which are not necessarily
unique) or object IDs, which are. Each object is given a unique object ID,
assigned sequentially throughout any given Praat session. These cannot be reset
or reused (other than by restarting Praat). Prefer these to names for selection.























