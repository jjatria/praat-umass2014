\documentclass[12pt]{beamer}

\usepackage{ifthen}
\usepackage{xcolor}

\newboolean{dark}
\setboolean{dark}{false}

\usepackage{polyglossia}
\setmainlanguage{english}

\usepackage{fontspec}
\setmainfont[Ligatures=TeX]{Linux Libertine O}
\setsansfont[Ligatures=TeX]{Linux Biolinum O}
\setmonofont{DejaVu Sans Mono}

% Japanese settings
\usepackage{xeCJK}
\setCJKmainfont{TakaoMincho}
\setCJKsansfont{TakaoGothic}
\usepackage{ruby}
\usepackage{ruby}
\renewcommand{\rubysep}{0ex}

\usepackage{relsize}

\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{pgfplotstable}
\pgfplotsset{compat=newest}
\usetikzlibrary{calc}
\usetikzlibrary{positioning}
\usetikzlibrary{matrix}
\usetikzlibrary{plotmarks}
\usetikzlibrary{shapes}

\definecolor{base03}{HTML}{002B36}
\definecolor{base02}{HTML}{073642}
\definecolor{base01}{HTML}{586E75}
\definecolor{base00}{HTML}{657B83}
\definecolor{base0}{HTML}{839496}
\definecolor{base1}{HTML}{93A1A1}
\definecolor{base2}{HTML}{EEE8D5}
\definecolor{base3}{HTML}{FDF6E3}
\definecolor{s_yellow}{HTML}{B58900}
\definecolor{s_orange}{HTML}{CB4B16}
\definecolor{s_red}{HTML}{DC322F}
\definecolor{s_magenta}{HTML}{d33682}
\definecolor{s_violet}{HTML}{6C71C4}
\definecolor{s_blue}{HTML}{268bd2}
\definecolor{s_cyan}{HTML}{2AA198}
\definecolor{s_green}{HTML}{859900}


\ifthenelse{\boolean{dark}}
{
  \setbeamercolor{title}{fg=white}
  \setbeamercolor{frametitle}{fg=white,bg=}
  \setbeamercolor{background canvas}{bg=black}
  \setbeamercolor{normal text}{fg=white}
  \setbeamercolor{block title}{fg=white}
  
  % Solarized dark
  \definecolor{listbg}     {named}{base03}
  \definecolor{normal}     {named}{base00}
  \definecolor{comment}    {named}{base01}
}
{
  \setbeamercolor{title}{fg=black}
  \setbeamercolor{frametitle}{fg=black,bg=}
  \setbeamercolor{background canvas}{bg=white}
  \setbeamercolor{normal text}{fg=black}
  \setbeamercolor{block title}{fg=black}
  
  % Solarized dark
  \definecolor{listbg}     {named}{base3}
  \definecolor{normal}     {named}{base0}
  \definecolor{comment}    {named}{base1}
}
% Solarized theme
\definecolor{defined_var}{named}{s_orange}
\definecolor{keyword}    {named}{s_green}
\definecolor{integer}    {named}{s_magenta}
\definecolor{string}     {named}{s_blue}
\definecolor{urlcolor}   {named}{s_blue}

\setbeamercolor{item}{fg=s_blue}
\setbeamertemplate{navigation symbols}{}

\usepackage{listings}
\lstdefinelanguage{Praat}{
  morekeywords=[0]{if, then, else, elif, elsif, endif, fi, for, from, to, endfor, procedure, endproc, while, endwhile, repeat, until, form, endform},
  keywordstyle=[0]\color{keyword},
  morekeywords=[1]{undefined, pi, e, newline\$, tab\$, praatVersion, praatVersion\$},
  keywordstyle=[1]\color{defined_var},
  morekeywords=[2]{appendInfoLine, selectObject, removeObject, plusObject, minusObject},
  keywordstyle=[2]\bfseries,
  sensitive=true,
  comment=[l]{;},
  morecomment=[f][commentstyle][0]{\#},
  morecomment=[f][commentstyle][2]{\#},
  morecomment=[f][commentstyle][4]{\#},
  commentstyle=\color{comment},
  stringstyle=\color{string},
  morestring=[b]',
  morestring=[b]"
}
\lstset{
  language=Praat,
  backgroundcolor=\color{listbg},
  extendedchars=true,
  basicstyle=\color{normal}\scriptsize\ttfamily,
  showstringspaces=false,
  showspaces=false,
  numbers=left,
  numberstyle=\color{gray}\tiny,
  numbersep=9pt,
  tabsize=2,
  gobble=2,
  breaklines=true,
  showtabs=false,
  captionpos=b,
  literate=%
    *{0}{{{\color{integer}0}}}1
    {1}{{{\color{integer}1}}}1
    {2}{{{\color{integer}2}}}1
    {3}{{{\color{integer}3}}}1
    {4}{{{\color{integer}4}}}1
    {5}{{{\color{integer}5}}}1
    {6}{{{\color{integer}6}}}1
    {7}{{{\color{integer}7}}}1
    {8}{{{\color{integer}8}}}1
    {9}{{{\color{integer}9}}}1
}

\usepackage{hyperref}
\hypersetup{colorlinks=true,linkcolor=,urlcolor=urlcolor}

\newcommand{\tikzmark}[1]{\tikz[overlay,remember picture] \node (#1) {};}

\AtBeginSection{
  \begin{frame}
  \begin{center}
  Part \thesection
  
  \Huge \insertsection
  \end{center}
  \end{frame}
}

\author{José Joaquín Atria\texorpdfstring{ (UCL){\\\small\href{mailto:j.atria.11@ucl.ac.uk}{j.atria.11@ucl.ac.uk}}}{}}
\title{Praat Scripting Primer}
\date{November 21\textsuperscript{st}, 2014}

\begin{document}
    
\maketitle
    
\begin{frame}[fragile]{Contents}

  \begin{itemize}
    \item Introduction: why and how?
    \begin{itemize}
      \item Good practices
    \end{itemize}
    \item The language
    \begin{itemize}
      \item Variables
      \item Booleans
      \item Control structures
      \item Object handling
      \item Commands, functions, procedures
      \item Formulas
      \item Bonus Hack: implementing dictionaries
    \end{itemize}
    \item Extensions
    \begin{itemize}
      \item Plugins
    \end{itemize}
  \end{itemize}

\end{frame}
    
\section{Introduction}
    
\begin{frame}[fragile]{Why use scripts?}

  They are
  \begin{itemize}
    \item precise
    \item reusable
    \item automatic
    \item portable
  \end{itemize}
  and so they are the best friends of science

\end{frame}
    
\begin{frame}[fragile]{What is a (Praat) script?}
  \begin{itemize}
    \item A set of instructions
    \item Stored as a text file
    \item Loaded and interpreted by Praat
  \end{itemize}
\end{frame}
    
\begin{frame}[fragile]{What can you do with a script?}
  Most of what you can do without one \ldots\ and more
  \begin{itemize}
    \item Automatise tedious tasks
    \item Invoke other scripts
    \item Modify the behaviour of Praat
    \item etc \ldots
  \end{itemize}
\end{frame}
    
\begin{frame}[fragile]{How do you make one?}
  With a text editor:
  \begin{itemize}
    \item \href{http://www.notepad-plus-plus.org/}{Notepad++} (Windows)
    \item \href{http://www.sublimetext.com/}{Sublime Text} (Multiplatform)
    \item \href{http://kate-editor.org/}{Kate} (KDE) or \href{http://www.geany.org/}{Geany} (GTK)
    \item \href{http://ace.c9.io/}{Ace} (Web)
    \item \href{http://www.barebones.com/products/textwrangler/}{TextWrangler} (Mac)
    \item Praat's own internal editor
  \end{itemize}
  
  Most of these have Praat syntax highlighting rules
  
  {\small\color{comment}(Interestingly, not Praat's editor)}
\end{frame}
    
\begin{frame}[fragile]{How do you make one?}
  Use the command history in Praat's editor
  
  \begin{figure}
    \includegraphics[scale=0.8]{praat_history.png}
  \end{figure}
\end{frame}
    
\begin{frame}[fragile]{How do you make a \emph{good} one?}
  In decreasing order of importance:
  \begin{itemize}
    \item Is it easy to read?
    \item Is it clear in what it does?
    \item Is it versatile?
    \item Does it work?
    \item Is it robust?
    \item Is it efficient?
  \end{itemize}
\end{frame}
    
\begin{frame}[fragile]{How do you make a \emph{good} one?}
  \begin{block}{Is it easy to read?}
    \begin{itemize}
      \item Does it have a consistent style?
      \item Is it properly indented?
      \item Are variable names informative?
      \item Is it thoroughly commented?
      \item Is it easily understood by others?
      \item Can you return to it in six months?
    \end{itemize}
  \end{block}
\end{frame}
    
\begin{frame}[fragile]{How do you make a \emph{good} one?}
  \begin{block}{Is it clear?}
    \begin{itemize}
      \item Does it have a clear objective?
      \item Is it transparent in how it achieves it?
      \item Is it well structured?\phantom{p}
    \end{itemize}
  \end{block}
\end{frame}
    
\begin{frame}[fragile]{How do you make a \emph{good} one?}
  \begin{block}{Is it versatile?}
    \begin{itemize}
      \item How generic is its design?
      \item Can it be used in other situations?
      \item Does it play well with others?
    \end{itemize}
  \end{block}
\end{frame}
    
\begin{frame}[fragile]{How do you make a \emph{good} one?}
  \begin{block}{Does it work?}
    \begin{itemize}
      \item[] An easily understood and well structured script that doesn't work can be fixed
      \item[] One that works in cryptic ways like a black box will eventually break down, and become useless
    \end{itemize}
  \end{block}
\end{frame}
    
\begin{frame}[fragile]{How do you make a \emph{good} one?}
  \begin{block}{Is it robust?}
    \begin{itemize}
      \item How well does it react to user errors?
      \item How well does it react to faulty data?
      \item Does it fail gracefully, or die screaming?
    \end{itemize}
  \end{block}
\end{frame}
    
\begin{frame}[fragile]{How do you make a \emph{good} one?}
  \begin{block}{Is it efficient?}
    \begin{itemize}
      \item Are the methods it uses the most appropriate?
      \item Are they the fastest?
      \item Are they the most cost-efficient?
      \item How much readability needed to be sacrificed?
    \end{itemize}
  \end{block}
\end{frame}

\section{The Scripting Language}

\begin{frame}[fragile]{Variables}

  Variables are losely typed:
  \begin{itemize}
    \item strings
    \item numeric  
  \end{itemize}

  \begin{lstlisting}[escapechar=\%]
  string$ = "He said "%\tikzmark{quote 1}%"string"%\tikzmark{quote 2}%" again"
  numeric = 42
  arrays#%\tikzmark{array}%
  \end{lstlisting}

  Arrays are still in development
  \begin{itemize}
    \item The closest thing today are ``indexed variables''
  \end{itemize}

  \begin{lstlisting}
  index[1]  = 1
  index[2]  = undefined
  index[10] = 10
  # index[3] does not exist

  index$[1] = "this is a different variable"
  \end{lstlisting}

  \begin{tikzpicture}[
      overlay,
      remember picture,
      shift={(current page.north east)}
    ]
    \path (quote 1) ++(0,0.25\baselineskip) node (A) {};
    \path (quote 2) ++(0,0.25\baselineskip) node (B) {};
    \path (array)   ++(0,0.2\baselineskip) node (C) {};
    \node (quote legend) at (-3.8, -2.9) {\scriptsize\color{comment}{escaped quotations}};
    \node (array legend) at (-3.8, -5.1) {\scriptsize\color{comment}{these are undocumented}};
    \draw[comment, thick, ->] (quote legend.west) to [out=160, in=90] (A.north);
    \draw[comment, thick, ->] (quote legend.west) to [out=160, in=90] (B.north);
    \draw[comment, thick, ->] (array legend.west) to [out=180, in=0]  (C);
  \end{tikzpicture}

\end{frame}

\begin{frame}[fragile]{Comments and lines}

There are two types of comments: line, and inline

  \begin{lstlisting}
  # Full line comment
  a = 10 ; Inline comment
  \end{lstlisting}

Lines can be broken with \lstinline[basicstyle=\smaller\ttfamily]$...$ at beginning

  \begin{lstlisting}[escapechar=\%]
  string$ = "electro
    ...encephalo
    ...gram"
  # is the same as
  string$ = "electroencephalogram"
  \end{lstlisting}

Avoid trailing spaces!

  \begin{minipage}{0.4\linewidth}
    \begin{lstlisting}[numbers=none, gobble=4]
    string$ = "Hello
      ... World!"
    \end{lstlisting}
  \end{minipage}
  \hfill > \hfill
  \begin{minipage}{0.4\linewidth}
    \begin{lstlisting}[numbers=none, gobble=4]
    string$ = "Hello␣
      ...World!"
    \end{lstlisting}
  \end{minipage}
\end{frame}
 
\begin{frame}[fragile]{Booleans}

  Cannot test \lstinline[basicstyle=\smaller\ttfamily]$undefined$ values or unknown variables

  \begin{lstlisting}
  assert newvar      ; Error: newvar does not exist
  newvar = undefined
  assert newvar      ; Error: it exists, but is undefined
  \end{lstlisting}

  There are three logical operators:
  \begin{itemize}
    \item \lstinline[basicstyle=\smaller\ttfamily]$!$ or \lstinline[basicstyle=\smaller\ttfamily]$not$, for negation
    \item \lstinline[basicstyle=\smaller\ttfamily]$&$ or \lstinline[basicstyle=\smaller\ttfamily]$and$, for conjunction
    \item \lstinline[basicstyle=\smaller\ttfamily]$|$ or \lstinline[basicstyle=\smaller\ttfamily]$or$, for disjunction
  \end{itemize}
  These operators short-circuit
    
  \begin{lstlisting}
  if var != undefined
    if var < 10
      # this can be made shorter
    endif
  endif

  if var != undefined and var < 10
    # short-circuiting makes this possible
  endif
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Booleans}
  Only numeric evaluated as boolean
  \begin{itemize}
    \item \lstinline[basicstyle=\smaller\ttfamily]$0$ evaluates to false
    \item Everything else evaluates to true
  \end{itemize}

  \begin{lstlisting}[escapechar=\%]
  assert !0 ; ! is the negation operator
  assert  1
  ass%\tikzmark{assert}%ert -1
  \end{lstlisting}

  \begin{tikzpicture}[
      overlay,
      remember picture,
      shift={(current page.north east)}
    ]
    \path (assert) ++(0,-0.2\baselineskip) node (A) {};
    \node (assert legend) at (-8.5,-5.75) {\scriptsize\color{comment}{halt if not true}};
    \draw[thick,comment,->] (assert legend) to [out=180,in=270] (A);
  \end{tikzpicture}

  Strings are not booleans, but can be used in conditions
 
  \begin{lstlisting}
  name$ = "John"
  if name$ != ""
    appendInfoLine: "Hello, ", name$
  endif
  \end{lstlisting}
\end{frame}
 
\begin{frame}[fragile]{Operators}
  \vfill
  \begin{tikzpicture}[remember picture]
    \matrix (operators) [matrix of nodes,
        nodes={
          baseline,
          circle,
          minimum size=1cm,
        },
        nodes in empty cells=true,
      ]
    {
      & & & \\
      & & & \\
      & & & \\
      & & & \\
    };
    
    \node at (operators-1-1) {\lstinline[basicstyle=\smaller\ttfamily]$=$};
    \node at (operators-1-2) {\lstinline[basicstyle=\smaller\ttfamily]$==$};
    \node at (operators-1-3) {\lstinline[basicstyle=\smaller\ttfamily]$<>$};

    \node at (operators-2-1) {\lstinline[basicstyle=\smaller\ttfamily]$>$};
    \node at (operators-2-2) {\lstinline[basicstyle=\smaller\ttfamily]$>=$};
    \node at (operators-2-3) {\lstinline[basicstyle=\smaller\ttfamily]$<=$};
    \node at (operators-2-4) {\lstinline[basicstyle=\smaller\ttfamily]$<$};

    \node at (operators-3-1) {\lstinline[basicstyle=\smaller\ttfamily]$+$};
    \node at (operators-3-2) {\lstinline[basicstyle=\smaller\ttfamily]$-$};
    \node at (operators-3-3) {\lstinline[basicstyle=\smaller\ttfamily]$*$};
    \node at (operators-3-4) {\lstinline[basicstyle=\smaller\ttfamily]$/$};

    \node at (operators-4-1) {\lstinline[basicstyle=\smaller\ttfamily]$^$};
    \node at (operators-4-2) {\lstinline[basicstyle=\smaller\ttfamily]$mod$};
    \node at (operators-4-3) {\lstinline[basicstyle=\smaller\ttfamily]$div$};
  \end{tikzpicture}
  
  \begin{tikzpicture}[overlay, remember picture]
    \only<1>{
  
      \draw[thick] (operators-1-1) circle [radius=0.4cm];
      \draw[thick] (operators-1-2) circle [radius=0.4cm];
      \draw[thick] (operators-1-3) circle [radius=0.4cm];
      
      \matrix (legend) [matrix of nodes, matrix anchor=center, row sep=0.5cm, right=4cm of operators, nodes={align=center}]
      {
        assignment / equality \\
        equality              \\
        inequality            \\
      };
      
      \draw[thick, ->] (legend-1-1) to [out=135, in=45, looseness=0.5] (operators-1-1);
      \draw[thick, ->] (legend-2-1) to [out=180, in=35]                (operators-1-2);
      \draw[thick, ->] (legend-3-1) to [out=180, in=300]               (operators-1-3);
    }
    
    \only<2>{
      \draw[thick] (operators-2-1) circle [radius=0.4cm];
      \draw[thick] (operators-2-2) circle [radius=0.4cm];
      \draw[thick] (operators-2-3) circle [radius=0.4cm];
      \draw[thick] (operators-2-4) circle [radius=0.4cm];
      
      \matrix (legend) [
          matrix of nodes,
          matrix anchor=center,
          row sep=0.5cm,
          right=4cm of operators,
          nodes={align=center},
        ]
      {
        greater than             \\
        greater than or equal to \\
        less than                \\
        less than or equal to    \\
      };
      
      \draw[thick, ->] (legend-1-1) to [out=135, in=70]  (operators-2-1);
      \draw[thick, ->] (legend-2-1) to [out=160, in=45]  (operators-2-2);
      \draw[thick, ->] (legend-3-1) to [out=180, in=330] (operators-2-4);
      \draw[thick, ->] (legend-4-1) to [out=180, in=300] (operators-2-3);
    }
    
    \only<3>{
      \draw[thick] (operators-3-1) circle [radius=0.4cm];
      \draw[thick] (operators-3-2) circle [radius=0.4cm];
      \draw[thick] (operators-3-3) circle [radius=0.4cm];
      \draw[thick] (operators-3-4) circle [radius=0.4cm];
      
      \matrix (legend) [matrix of nodes, matrix anchor=center, row sep=0.5cm, right=4cm of operators, nodes={align=center}]
      {
        addition       \\
        subtraction    \\
        division       \\
        multiplication \\
      };
      
      \draw[thick, ->] (legend-1-1) to [out=180, in=75]  (operators-3-1);
      \draw[thick, ->] (legend-2-1) to [out=180, in=60]  (operators-3-2);
      \draw[thick, ->] (legend-3-1) --                   (operators-3-4);
      \draw[thick, ->] (legend-4-1) to [out=180, in=300] (operators-3-3);
    }
    
    \only<4>{
      \draw[thick] (operators-4-1) circle [radius=0.4cm];
      \draw[thick] (operators-4-2) circle [radius=0.4cm];
      \draw[thick] (operators-4-3) circle [radius=0.4cm];
      
      \matrix (legend) [matrix of nodes, matrix anchor=center, row sep=0.5cm, right=4cm of operators, nodes={align=center}]
      {
        exponentiation   \\
        modulo           \\
        integer division \\
      };
      
      \draw[thick, ->] (legend-1-1) to [out=160, in=90]                 (operators-4-1);
      \draw[thick, ->] (legend-2-1) to [out=180, in=75]                 (operators-4-2);
      \draw[thick, ->] (legend-3-1) to [out=200, in=330, looseness=0.6] (operators-4-3);
    }
  \end{tikzpicture}
  
  \begin{overlayarea}{\textwidth}{4\baselineskip}
    \only<1>{Difference between \lstinline[basicstyle=\smaller\ttfamily]$=$ and \lstinline[basicstyle=\smaller\ttfamily]$==$ is not enforced by Praat}
    \only<4>{Result of modulo is signed like divisor}
  \end{overlayarea}
\end{frame}

\begin{frame}[fragile]{Operators: at a glance}
  \begin{lstlisting}
  assert   1  =  1
  assert   1  == 1
  assert   1  <> 2
  assert   2  >  1
  assert   2  >= 2
  assert   1  <= 1
  assert   1  <  2
  assert   3  =  1  +    2  ; addition
  assert   1  =  1  -    0  ; substraction
  assert  -1  =  1  *   -1  ; multiplication
  assert 0.5  =  1  /    2  ; division
  assert   8  =  2  ^    3  ; exponentiation
  assert   4  =  16 mod  12 ; modulo
  assert  -8  =  16 mod -12 ; modulo is signed like divisor
  assert   8  = -16 mod  12
  assert   1  =  3  div  2  ; integer division
  assert !(1 !=  1)
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{String operations}

  Operators behave differently for strings and numerics
  
  Strings and numerics cannot be mixed

  \begin{lstlisting}
  assert "hello" =  "he" + "llo"
  assert "hello" =  "hello world" - " world"
  assert "hello" =  "hello" - "bye"
  assert "beta"  >  "alpha"
  assert "beta"  >= "alpha"
  assert "ALPHA" <= "alpha"
  assert "ALPHA" <  "alpha"
  assert "hello" != "hel10"

  assert "1212" = string$(12) + "12"
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Control structures: 
  \lstinline[basicstyle=\smaller\ttfamily]$for$ loops%
}

  \begin{block}{General syntax}
    \begin{lstlisting}[gobble=4]
    for x [from y] to z
      # code
    endfor
    \end{lstlisting}
  \end{block}

  \begin{block}{Example}
    \begin{lstlisting}[gobble=4]
    clearinfo
    for number to 10
      count = 10 - number
      appendInfoLine: count, "..."
    endfor
    appendInfoLine: "Liftoff!"
    \end{lstlisting}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Control structures: 
  \lstinline[basicstyle=\smaller\ttfamily]$if$ blocks%
}

  \begin{block}{General syntax}
    \begin{lstlisting}[gobble=4,%
      literate={1}{{{\color{normal}1}}}1
               {2}{{{\color{normal}2}}}1]
    if cond1
      # code
    [elsif cond2
      # code]
    [else
      # code]
    endif
    \end{lstlisting}
  \end{block}

  \begin{block}{Example}
    \begin{lstlisting}[gobble=4]
    for number to 10
      if number < 5
        appendInfoLine: "First half..."
      elsif number > 5
        appendInfoLine: "Second half..."
      else  
        appendInfoLine: "Halfway through!"
      endif
    endfor
    \end{lstlisting}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Control structures: 
  inline \lstinline[basicstyle=\smaller\ttfamily]$if$%
}

  \begin{block}{General syntax}
    \begin{lstlisting}[gobble=4]
    if cond then ... else ... fi
    \end{lstlisting}
  \end{block}

  \begin{block}{Example}
    \begin{lstlisting}[gobble=4]
    for number to 10
      appendInfoLine:
        ... if number < 5 then "First half"  else
        ... if number > 5 then "Second half" else
        ... "Halfway through!" fi fi ; note the double fi
    endfor
    number = if number = undefined then 0 else number fi
    \end{lstlisting}
  \end{block}

  \begin{itemize}
    \item Used heavily in formulas
    \item \emph{Must} be fully specified
    \item Can be nested
    \item Can be ended by \lstinline[basicstyle=\smaller\ttfamily]$endif$
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Control structures: 
  \lstinline[basicstyle=\smaller\ttfamily]$while$ blocks%
}

  \begin{block}{General syntax}
    \begin{lstlisting}[gobble=4]
    while cond
      #code
    endwhile
    \end{lstlisting}
  \end{block}

  \begin{block}{Example}
    \begin{lstlisting}[gobble=4]
    clearinfo
    number = randomInteger(1, 300000)
    appendInfoLine: "Number starts as ", number
    while number != 10
      if number > 10
        number = round(number / 2)
      elsif number < 10
        number = round(number * 5)
      endif
      appendInfoLine: "...and now it is ", number, "..."
    endwhile
    \end{lstlisting}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Control structures: 
  \lstinline[basicstyle=\smaller\ttfamily]$repeat$ blocks%
} 

  \begin{block}{General syntax}
    \begin{lstlisting}[gobble=4]
    repeat
      #code
    until cond
    \end{lstlisting}
  \end{block}

  \begin{block}{Example}
    \begin{lstlisting}[gobble=4]
    clearinfo
    number = randomInteger(1, 300000)
    appendInfoLine: "Number starts as ", number
    repeat
      if number > 10
        number = round(number / 2)
      elsif number < 10
        number = round(number * 5)
      endif
      appendInfoLine: "...and now it is ", number, "..."
    until number = 10
    \end{lstlisting}
  \end{block}

\end{frame}

\begin{frame}[fragile]{Object handling}
  \begin{itemize}
    \item Why?
    \begin{itemize}
      \item Commands depend on current selection
    \end{itemize}
    \item How?
    \begin{itemize}
      \item By name or by object ID
      \item Name may not be unique
      \item Prefer IDs
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Object handling}

  \begin{block}{Commands}
    \begin{lstlisting}[gobble=4,
      literate={1}{{{\color{normal}1}}}1
               {2}{{{\color{normal}2}}}1]
    selectObject: id1[,id2,...] ; select specified objects
    removeObject: id1[,id2,...] ; remove specified objects
    plusObject:   id1[,id2,...] ; add objects to selection
    minusObject:  id1[,id2,...] ; remove objects from selection
    \end{lstlisting}
  \end{block}

  \begin{block}{Functions}
    \begin{lstlisting}[gobble=4]
    selected (type$, index) ; returns id of selected object
    selected$(type$, index) ; returns name of selected object
    numberOfSelected(type$) ; returns number of selected objects
    \end{lstlisting}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Object handling: a full example}

  \begin{lstlisting}
  n = numberOfSelected()
  if n
    # save original selection
    for i to n
      obj[i] = selected(i)
    endfor

    # for each originally selected object
    for i to n
      selectObject(obj[i])
      type$ = extractWord$(selected$(), "") ; get object type
      name$ = selected$(type$)              ; and name
      if type$ = "Sound" ; could have been if selected("Sound")
        Reverse
      endif
    endfor

    # restore original selection
    selectObject: obj[1]
    for i from 2 to n
      plusObject: obj[i]
    endfor
  endif
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Commands}
  \begin{itemize}
    \item All buttons in Praat execute a command
    \item \emph{Most} commands in Praat are available from a script
    \item They allow us to
    \begin{itemize}
      \item Manipulate objects (creation, deletion, edition, \ldots)
      \item Query information about them
    \end{itemize}
  \end{itemize}

  \begin{lstlisting}[emph={from},emphstyle={\color{normal}}]
  sound = Create Sound from formula: "sound",
    ... 1, 0, 1, 44100,
    ... "sin(2 * pi * 440 * x)"
  duration = Get total duration
  quarter = duration / 4
  textgrid = To TextGrid: "points", "points"
  for i to 3
    point = quarter * i
    Insert point: 1, point, string$(i)
  endfor
  selectObject: sound, textgrid
  View & Edit
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Functions}
  \begin{itemize}
    \item Encapsulated functionality
    \item \emph{All} functions return a (maybe useless) value
    \item Return type is specified in name
    \item Might be overloaded
    \item Cannot be on the left of an assignment
  \end{itemize}
  
  \begin{lstlisting}
  left$(str$, len)                   ; left substring 
  right$(str$, len)                  ; right substring
  mid$(str$, idx, len)               ; substring
  index(src$, tgt$)                  ; search in string
  index_regex(str$, ptn$)            ; search with regex
  replace_regex(str$, ptn$, new$, n) ; replace with regex
  date()                             ; preformatted date
  string$(num)                       ; stringify numeric
  number(str$)                       ; numerify string
  chooseReadFile(lbl$)               ; input file selector
  chooseWriteFile(lbl$, fnm$)        ; output file selector
  chooseDirectory$(lbl$)             ; directory selector
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Procedures}
  Procedures are similar to user-defined functions
  \begin{itemize}
    \item They encapsulate blocks of code
    \item They can receive arguments and provide values
  \end{itemize}
  But they are \emph{not} the same
  \begin{itemize}
    \item They do not return values
    \item They cannot accept operators
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Procedures}
  \begin{block}{General syntax}
    \begin{lstlisting}[gobble=4,
      literate={1}{{{\color{normal}1}}}1
               {2}{{{\color{normal}2}}}1]
    procedure my_procedure: [.arg1, .arg2, ...] ; definition
      #code
    endproc
    @my_procedure: [arg1, arg2, ...]            ; call
    \end{lstlisting}
  \end{block}

  \begin{block}{Example: with arguments}
    \begin{lstlisting}[escapechar=\%, gobble=4]
    @measure: "hello"
    @measure: "test"
    @measure: "electroencephalogram"

    procedure measure: .word$
      appendInfoLine: .word$, " has ", length(.word$), " characters"
    endproc

    %~%
    \end{lstlisting}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Procedures}
  \begin{block}{General syntax}
    \begin{lstlisting}[gobble=4]
    procedure my_procedure: [.arg1, .arg2, ...] ; definition
      #code
    endproc
    @my_procedure: [arg1, arg2, ...]            ; call
    \end{lstlisting}
  \end{block}

  \begin{block}{Example: with ``return'' values}
    \begin{lstlisting}[gobble=4]
    vowels$ = "aeiou"
    for i to length(vowels$)
      @getChar(i, vowels$)
      appendInfoLine: "Vowel ", i, " is """, getChar.c$, """"
    endfor

    procedure getChar: .index, .string$
      .c$ = mid$(.string$, .index, 1)
    endproc
    \end{lstlisting}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Procedures: Recursivity}

  Procedures can be recursive (up to 50 levels)
  
  \begin{lstlisting}
  # This does _not_ work!
  procedure factorial: .n
    if !.n
      .a = 1
    else
      @factorial: .n-1
      .a = .n * factorial.a
    endif
  endproc
  \end{lstlisting}

  But \lstinline[basicstyle=\smaller\ttfamily]$.var$ is the same as \lstinline[basicstyle=\smaller\ttfamily]$myprocedure.var$
  
  And \emph{all} variables share the same scope
  
  On every call \lstinline[basicstyle=\smaller\ttfamily]$.var$ gets overwritten!
\end{frame}

\begin{frame}[fragile]{Procedures: Recursivity}
  
  Local scope must be manually enforced!
  
  \begin{lstlisting}
  # This _does_ work
  procedure factorial: .n
    # Cannot use an inline if here
    if !variableExists("factorial.level")
      .level = 1
    else
      .level += 1
    endif
    .n[.level] = .n

    if !.n
      .a[.level] = 1
    else
      @factorial: .n-1
      .a[.level] = .n[.level] * factorial.a[.level+1]
    endif
    .level -= 1
  endproc
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Formulas}
  Whatever can go on the right side of an assignment
  
  Some commands accept them too

  \begin{lstlisting}[
    emph={from},
    emphstyle={\color{normal}}
  ]
  Create Sound from formula: ... ; for samples in time
  Create Matrix:                 ; for cell contents
  Create simple Matrix:          ; for cell contents
  Create Photo:                  ; for R G B matrices
  Create simple Photo:           ; for R G B matrices
  Sound -> Formula:              ; for sample manipulation
  Sound -> Formula (part):       ; for sample manipulation
  ...
  \end{lstlisting}

  They can be tested in the calculator
  \begin{itemize}
    \item[] Praat \textrightarrow\ Goodies \textrightarrow\ Calculator...
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Formulas}
  Formulas work on matrices
  
  Many objects are matrices under the hood
  \begin{itemize}
    \item Sound: samples on $x$, channels on $y$
    \item Pitch: frames on $x$, one-dimensional
    \item Spectrum: frequency bins on $x$, real and imaginary on $y$
    \item Spectrogram: frames on $x$, frequency bands on $y$
    \item \ldots
  \end{itemize}
  
  Object attributes can be accessed directly

  \begin{minipage}{0.49\linewidth}
    \begin{lstlisting}[numbers=none, gobble=4]
    selectObject: "Sound hello"
    start = Get start time

    selectObject: 1
    duration = Get total duration
    \end{lstlisting}
  \end{minipage}
  \begin{minipage}{0.49\linewidth}
    \begin{lstlisting}[numbers=none, gobble=4,
      literate={1}{{{\color{normal}1}}}1]
    start = Sound_hello.xmin


    duration =
    ... Object_1.xmax-Object_1.xmin
    \end{lstlisting}
  \end{minipage}

  \begin{tikzpicture}[
      overlay,
      remember picture,
      shift={(current page.north east)},
    ]
    \draw[comment,thick,<-] (-4,-8.6) to [out=270, in=0] (-7,-9) node[left]
      {\color{comment}\scriptsize right side is \emph{much} faster}; 
  \end{tikzpicture}
\end{frame}

\begin{frame}[fragile]{Formulas}
  Attributes from any object are accessible in a formula
  
When creating an object:
  \begin{itemize}
    \item Attributes can be accessed without specifier
    \item Can be dissambiguated as eg. \lstinline[basicstyle=\smaller\ttfamily]$Self.xmin$
  \end{itemize}
  
  When modifying an object:
  
  \begin{itemize}
    \item \lstinline[basicstyle=\smaller\ttfamily]$self$ refers to the unit modified (eg. frame, sample, \ldots)
  \end{itemize}
  
  \begin{lstlisting}
  xmin    ; time start, low frequency
  xmax    ; time end, high frequency
  ncol    ; number of columns
  nrow    ; number of rows
  col$[i] ; name of ith column
  row$[i] ; name of ith row
  nx      ; ncol, Sound samples, Pitch frames, ...
  dx      ; sample period, Pitch frame time step, ...
  ymin    ; low frequency in Spectrogram, ...
  ymax    ; high frequency in Spectrogram, ...
  ny      ; nrow, channels in Sound, formants in Formant, ...
  dy      ; distance between rows
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Bonus: hacking a dictionary}
  \begin{itemize}
    \item Praat's indexed variables stand in for arrays
    \item But what about dictionaries?
    \begin{itemize}
      \item In an array, access is by index
      \item In a dictionary, access is by \emph{key}
    \end{itemize}
    \item Dictionaries are useful for eg. keeping counts
    \item Praat does \emph{not} have dictionaries
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Bonus: hacking a dictionary}
  \begin{itemize}
    \item But it does have \emph{Tables}!
  \end{itemize}
  
  \begin{lstlisting}
    Create Table without column names: "dictionary", 1, 1
    string$ = "aoeuiaouieieouaiuueiuoieoaeieoaeuuiae"
    
    for i to length(string$)
      @getChar: i, string$
      key$ = getChar.c$
      exists = Get column index: key$
      if exists                            ; key is known
        n = Get value: 1, key$
        Set numeric value: 1, key$, n+1
      else                                 ; key is new
        Append column: key$ 
        Set numeric value: 1, key$, 1
      endif
    endfor
    
    procedure getChar: .index, .string$
      .c$ = mid$(.string$, .index, 1)
    endproc
  \end{lstlisting}
\end{frame}

\section{Extensions}

\begin{frame}[fragile]{Plugins}
  \begin{itemize}
    \item Distributable package for a set of scripts
    \item Simple installation and removal
    \begin{itemize}
      \item They are saved in Praat's preferences directory
    \end{itemize}
    \item Packaged scripts can be used independently
    \item Particularly useful for tools for teaching and research
  \end{itemize}  
\end{frame}

% \section{The Demo Window}
% 
% \begin{frame}[fragile]{Pauses}
% \end{frame}
% 
% \begin{frame}[fragile]{Demo Window}
% \end{frame}

\begin{frame}[fragile]{Other Resources}
  \begin{block}{Plugins}
    \begin{itemize}
     \item \href{https://github.com/jjatria/plugin_jjatools}{JJATools}: my tools and extensions to Praat
    \end{itemize}
  \end{block}
     
  \begin{block}{Syntax highlighters}
    \begin{itemize}
     \item \href{https://github.com/jjatria/praatKateSyntax}{Kate}, by Joaquín Atria
     \item \href{https://github.com/mauriciofigueroa/praatSublimeSyntax}{Sublime Text}, by Mauricio Figueroa
     \item \href{http://sadowsky.cl/praat.html#syntax}{Notepad++}, by Scott Sadowsky
    \end{itemize}
  \end{block}
     
  \begin{block}{Manuals}
    \begin{itemize}
     \item \href{http://www.fon.hum.uva.nl/praat/manual/Scripting.html}{Praat's own tutorial}
     \item \href{http://www.mauriciofigueroa.cl/04_scripts/04_scripts.html}{Praat scripting for beginners}, by Mauricio Figueroa
    \end{itemize}
  \end{block}

\end{frame}

\end{document}
